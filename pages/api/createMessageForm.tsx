import { sanityClient } from "@/sanity";
import type { NextApiRequest, NextApiResponse } from "next";


export default async function createContactMessage(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { firstname, lastname, email, message,  } = JSON.parse(
    req.body
  );
  
  try {
    await sanityClient.create({
      _type: "contact",
      firstname,
      lastname,
      email,
      message,
    });
  } catch (err) {
    return res
      .status(500)
      .json({ message: `Couldn't submit contact form`, err });
  }
  

  return res.status(200).json({ message: "message sent" });
}