// pages/404.tsx
import Layout from '@/src/components/Layout/Layout';
import React from 'react';

const NotFound = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-purple-700 text-white">
      <div className="text-center">
        <h1 className="text-6xl font-bold mb-4">404</h1>
        <p className="text-2xl mb-8">Page Not Found</p>
        <a href="/" className="text-blue-300 underline">
          Go back to homepage
        </a>
      </div>
    </div>
  );
}

export default NotFound;
