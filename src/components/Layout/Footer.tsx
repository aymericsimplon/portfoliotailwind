import React from "react";

function Footer() {
  return (
    <footer className="w-full bg-gradient-to-r from-purpleouais to-purplee text-white py-6">
      <div className=" mx-auto px-8 flex flex-col md:flex-row justify-between items-center">
        <div className="mb-4 md:mb-0 text-center md:text-left">
          <h2 className="text-xl font-bold">Mon Portfolio</h2>
          <p>&copy; 2024 Tous droits réservés.</p>
        </div>
        <div className="flex space-x-4">
          <a href="https://www.linkedin.com" className="hover:text-gray-400">LinkedIn</a>
          <a href="https://www.github.com" className="hover:text-gray-400">GitHub</a>
          <a href="mailto:example@example.com" className="hover:text-gray-400">Contact</a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
