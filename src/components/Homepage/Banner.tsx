import Link from "next/link";
import React from "react";

function Banner() {
  return (
    <main className="w-full h-2/5 bg-purplehello">
      {/* Titre */}
      <div className="flex ml-16 py-5 font-bold text-4xl">
        <h2>Hello,</h2>
      </div>
      {/* Sous-titre*/}
      <div className="flex ml-16 text-5xl text-[#8F72B5] font-sans">
        <p>Je suis développeur web</p>
      </div>
      <span className="flex ml-16 text-2xl text-white">
        Développeur passionné par le web et l'innovation
      </span>
      {/* Logo */}
      <div className="flex flex-row"></div>

      {/* Bannière exp etc en gros y'a le bg */}
      <div className="w-full h-1/4 my-36 bg-purplee flex justify-start gap-y-2">
        <p className="   text-white text-2xl">Techno récemment utilisés</p>
      </div>

      {/* Logo des langages utilisé récemment*/}
      <div>
        
      </div>
    </main>
  );
}

export default Banner;

