import { urlFor } from "@/sanity";
import { Project } from "@/typing";
import Image from "next/image";
import Link from "next/link";
import React from "react";

interface Props{
  project: Project;
}

function HomepageCardService({project}:Props) 
{
  return (
    <div className="w-full flex flex-col bg-white rounded-md overflow-hidden">
      
      {project?.photo &&
      <div className="relative w-full h-72 flex-shrink-0">
        <Image
          src={urlFor(project?.photo).url()!}
          fill
          className="object-cover"
          alt="fille magnifique"
        />
      </div>
}

      <div className="text-neutral-700 p-5 flex flex-col gap-3">
        <h3 className="font-bold text-lg">{project?.title}</h3>
        <p>{project?.description}</p>
        <Link
        href={'/'}
        className="bg-purpledev hover:bg-purple-900 text-white font-bold py-1 px-2 rounded-lg focus:outline-none focus:shadow-outline flex items-center justify-center">
          View code
        </Link>
      </div>
    </div>
  );
}

export default HomepageCardService;