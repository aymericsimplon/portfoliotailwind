import React from 'react'
import ProjectsListItem from './ProjectsListItem'
import { Project } from '@/typing';

interface Props {
    projects: Project[];
  }

function Projects({projects}:Props) {
    
  return (
    <section className='flex w-full py-14 bg-whiteabtme'>
        <div className='w-5/6 mx-auto '>
            {/*Titre projets*/}

            <div className="flex flex-col gap-y-3 ">
                <h2 className="text-[#331C52] text-4xl ">Mes projets</h2>
                    <p className="text-[#331C52] text-2xl font-medium">Quelques projets que j'ai pu faire</p>
        </div>

        
        {/*Cards projets */}

            <ul className='grid grid-cols-3 gap-4  '>
            {projects?.map((project:any) => (
  <ProjectsListItem   key={project?._id} project={project}/>
))}

            </ul>
        </div>
        
    </section>
  )
}

export default Projects
