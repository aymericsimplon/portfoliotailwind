import { urlFor } from '@/sanity'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { FaGithub } from 'react-icons/fa'

function ProjectsListItem({project}:any) {

return (
    <li>

<div className="max-w-xs rounded-lg overflow-hidden shadow-lg bg-purplecard m-2">
            
            {project?.photo && 
            <div className='relative w-full aspect-square'>
            
            <Image
          src={urlFor(project?.photo).url()!}
          fill
          className="object-cover"
          alt="accueil du blog"
        /> </div>}

            <div className="px-3 py-2">
                <div className="font-bold text-md mb-1">{project?.title}</div>
                <p className="text-gray-700 text-sm mb-2">
                {project?.description}
                </p>
                <Link
                    href={`/projects/${project?.slug?.current}`}
                    className=" bg-purpledev hover:bg-purple-900 text-white font-bold py-1 px-2 rounded-lg focus:outline-none focus:shadow-outline flex items-center justify-center"
                >
                    <FaGithub className="text-lg mr-1" />
                    View code
                </Link>
            </div>
        </div>

    </li>
  )
}

export default ProjectsListItem

